import { r } from '@ioc:RethinkDb'
import { addCreatedAt } from 'App/modules/Helpers'

/**
 * Player model
 */
export default class Player {
  /** Player row UUID */
  public id: string
  /** Player unique ID */
  public uid: number
  /** Player in game name */
  public nickName: string
  /** Unix timestamp when player was first crawled */
  public createdAt: number
  /** Player table name */
  public static table = 'players'

  /**
   * Get player by nickName
   *
   * @param {String} nickName Player nickName
   *
   * @returns Promise that resolves into Player or null
   */
  public static byNickname (nickName: string): Promise<Player | null> {
    return r.table(this.table)
      .getAll(nickName, { index: 'nickName' })
      .run()
      .then(data => data[0] || null)
  }

  /**
   * Save player to DB if he doesn't exist already
   *
   * @param player Player to search or insert, search is done by uid
   *
   * @returns Promise that resolves into Player
   */
  public static async createIfNotExists (player: Player): Promise<Player> {
    let players = <Player[]>await r.table(this.table)
      .getAll(player.uid, { index: 'uid' })
      .limit(1)
      .run()

    if (players.length) {
      return players[0]
    }
    player = addCreatedAt(player)
    await r.table(this.table)
      .insert(this.clean(player))
      .run()

    return player
  }

  /**
   * Clean player object of all unneccessary data
   *
   * @param player Player object to clean
   *
   * @returns Clean player object
   */
  public static clean (player: Player): Player {
    let newPlayer = <Player>{
      uid: player.uid,
      nickName: player.nickName,
      createdAt: player.createdAt,
    }
    if (player.id) {
      newPlayer.id = player.id
    }

    return newPlayer
  }
}
