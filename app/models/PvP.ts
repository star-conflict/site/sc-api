/**
 * PvP model
 */
export default class PvP {
  /** Total PvP games played */
  public gamePlayed: number
  /** Nr of PvP games won */
  public gameWin: number
  /** Total nr of assists */
  public totalAssists: number
  /** Total battle time in MS */
  public totalBattleTime: number
  /** Total deaths */
  public totalDeath: number
  /** Total damage done */
  public totalDmgDone: number
  /** Total healing done */
  public totalHealingDone: number
  /** Total nr of kills */
  public totalKill: number
  /** Total capture points captured */
  public totalVpDmgDone: number
}
