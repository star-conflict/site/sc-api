/**
 * Clan model
 */
export default class Clan {
  /** Corporation unique ID */
  public cid: number
  /** Corporation name */
  public name: string
  /** Corporation total PvE rating */
  public pveRating?: number
  /** Corporation total PvP rating */
  public pvpRating?: number
  /** Corporation tag */
  public tag?: string
}
