/**
 * Coop model
 */
export default class Coop {
  /** Total coop games played */
  public gamePlayed: number
  /** Nr of coop games won */
  public gameWin: number
  /** Total battle time in MS */
  public totalBattleTime: number
}
