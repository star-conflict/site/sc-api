import { r } from '@ioc:RethinkDb'
import Clan from './Clan'
import Coop from './Coop'
import Player from './Player'
import OpenWorld from './OpenWorld'
import PvE from './PvE'
import PvP from './PvP'
import { addCreatedAt } from 'App/modules/Helpers'

/**
 * Player history model
 */
export default class PlayerHistory {
  /** Player level at given time */
  public accountRank?: number
  /** Pilot rating (player efficiency rating) at given time */
  public effRating?: number
  /** Player history row UUID */
  public id: string
  /** Player unique ID */
  public uid: Player['uid']
  /** Player in game name at given time */
  public nickName: Player['nickName']
  /** Player fleet power / 100 at given time */
  public prestigeBonus?: number
  /** Clan where player was at given time */
  public clan?: Clan
  /** Player coop games stats at given time */
  public coop?: Coop
  /** Player open space stats at given time */
  public openWorld?: OpenWorld
  /** Player PvE stat at given time */
  public pve?: PvE
  /** Player PvP stats at given time */
  public pvp?: PvP
  /** EPOCH timestamp when entry was made */
  public createdAt: number
  /** Player history table name */
  public static table = 'playerHistory'

  /**
   * Get player history by player UID
   *
   * @param uid Player UID to get history for
   * @param limit Number of rows to return
   *
   * @returns Promise that resolves into array of Players
   */
  public static byUid (uid: number, limit: number = 10): Promise<Player[]> {
    return r.table(this.table)
      .getAll(uid, { index: 'uid' })
      .orderBy(r.desc('createdAt'))
      .limit(limit)
      .run()
  }

  /**
   * Save player history into DB
   *
   * @param data Player history object
   */
  public static async create (data) {
    data = addCreatedAt(data)
    await r.table(this.table).insert(data).run()
    return data
  }

  /**
   * Save player history to DB if it doesn't exist already
   *
   * @param playerHistory Player history to search or insert, search is done by uid
   *
   * @returns Promise that resolves into PlayerHistory
   */
  public static async createIfNotExists (playerHistory: PlayerHistory) {
    let history = <PlayerHistory[]>await r.table(this.table)
      .getAll(playerHistory.uid, { index: 'uid' })
      .limit(1)
      .run()

    if (history.length) {
      return history[0]
    }

    let newHistory = <PlayerHistory>addCreatedAt(playerHistory)
    await r.table(this.table)
      .insert(newHistory)
      .run()

    return newHistory
  }

  /**
   * Save player history into DB without any checks
   *
   * Useful for fast mass inserts, but it might end up with corrupted data
   *
   * @param data Player history object
   */
  public static async createUnsafe (data) {
    data = addCreatedAt(data)
    await r.table(this.table, { readMode: 'outdated' })
      .insert(data, { conflict: 'replace', durability: 'soft' })
      .run()
    return data
  }
}
