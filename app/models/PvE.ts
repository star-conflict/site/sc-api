/**
 * PvE model
 */
export default class PvE {
  /** Total PvE games played */
  public gamePlayed: number
  /** Player PvE attack / power level */
  public attackLevel?: number
  /** Player PvE defence level */
  public defenceLevel?: number
  /** Player max PvE mission levels */
  public missionLevels?: {
    /** Defence Contract */
    defenceContract?: number, // nalnifan_lumen_waves_T1 in API
    /** The price of trust */
    thePriceOfTrust?: number, // pve_empfrontier_waves_T1 in API
    /** Ariadne's Thread */
    ariadnesThread?: number, // bigship_building_2_easy in API
    /** 'Blackwood' Shipyard */
    blackwoodShipyard?: number, // bigship_building_easy in API
    /** Operation 'Ice Belt' */
    operationIceBelt?: number, // asteroid_building_t1 in API
    /** Operation 'Monolith' */
    operationMonolith?: number, // pve_desttown_waves_easy in API
    /** Captured Dreadnought */
    capturedDreadnought?: number, // capture_repairbase_t1 in API
    /** Fire support */
    fireSupport?: number, // planet_war_waves_T1 in API
    /** Processing rig */
    processingRig?: number, // loot_geostation_normal in API
    /** 'Ellydium' plant raid */
    ellydiumPlantRaid?: number, // pve_frozen_station_t2 in API
    /** Operation «Crimson Haze» */
    operationCrimsonHaze?: number, // pve_jericho_base_t2 in API
    /** Temple of Last hope */
    templeOfLastHope?: number, // wavePve_maxWave in API
    /** Pirate Fort Raid */
    pirateFortRaid?: number, // piratebay_hard in API
  }
}
