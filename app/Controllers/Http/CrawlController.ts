// import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { r } from '@ioc:RethinkDb'
import ScApi from 'App/modules/ScApi'
import PlayerHistory from 'App/models/PlayerHistory'
import Player from 'App/models/Player'

let status = {
  crawled: 0,
  errors: 0,
  crawling: false,
}

export default class CrawlController {
  /**
   * Handles all the crawling from SC API
   */
  public async crawl () {
    // Set status
    status.crawled = 0
    status.errors = 0
    status.crawling = true
    // Get all players
    let players = await r.table(Player.table)
      .pluck('nickName')
      .run()
    let requests: Array<Promise<any>> = []
    const batchSize = 100
    // Start crawling in batches
    for (let i = 0; i < players.length; i++) {
      const player = players[i]
      // Get new data thro safe proxy
      const request = ScApi.fetchProxy(player.nickName)
        .then((newData) => {
          status.crawled++
          // Save new data to DB
          return PlayerHistory.createUnsafe(newData)
        })
        .catch(err => {
          status.errors++
          if ((err.error || {}).response) {
            return console.error((err.error || {}).response)
          }
          if (err.response) {
            return console.error(err.response)
          }
          if (err) {
            console.log(err)
          }
        })
      requests.push(request)
      if (requests.length > batchSize) {
        await Promise.all(requests)
        requests = []
      }
    }

    await Promise.all(requests)
    status.crawling = false
  }

  /**
   * Show current crawling status
   */
  public async status () {
    return status
  }

  /**
   * Start crawling in background
   */
  public start () {
    if (status.crawling) {
      return 'Crawler already running'
    }
    setImmediate(this.crawl)
    return 'Crawler started'
  }
}
