import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import PlayerHistory from 'App/models/PlayerHistory'

export default class PlayersController {
  /**
   * Get player history by player uid
   */
  public async history ({ params }: HttpContextContract) {
    return PlayerHistory.byUid(Number(params.id), 10)
  }
}
