// import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { Client } from 'pg'
import { r } from '@ioc:RethinkDb'
import PlayerHistory from 'App/models/PlayerHistory'
import Player from 'App/models/Player'
import { RawToNormal } from 'App/modules/DataMapper'

let status = {
  history: {
    total: 0,
    imported: 0,
    importing: false,
  },
  players: {
    total: 0,
    imported: 0,
    importing: false,
  },
}

export default class ImportsController {
  /**
   * Import player histories from postgres
   */
  public async importHistory () {
    status.history.importing = true
    // Get all raw history from PG
    let client = new Client({
      host: process.env.PG_HOST,
      user: process.env.PG_USER,
      database: process.env.PG_DB,
      password: process.env.PG_PW,
    })
    await client.connect()

    // Query data in chunks and work in chunks
    const countRes = await client.query('SELECT count(*) as count FROM player_histories')
    status.history.total = countRes.rows[0].count
    const chunkSize = 50000
    const chunks = Math.ceil(countRes.rows[0].count / chunkSize)
    for (let currentChunk = 0; currentChunk <= chunks; currentChunk++) {
      let query = {
        text: `SELECT id, raw, created_at FROM player_histories ORDER BY id LIMIT ${chunkSize} OFFSET ${currentChunk * chunkSize}`,
        rawMode: 'array',
      }
      const res = await client.query(query)
      res.rows = res.rows.map(row => {
        return {
          ...RawToNormal(row.raw),
          createdAt: (new Date(row.created_at)).getTime(),
        }
      })
      await PlayerHistory.createUnsafe(res.rows)
      status.history.imported = currentChunk * chunkSize
    }
    client.end()
    status.history.importing = false
  }

  /**
   * Import players from PG
   */
  public async importPlayers () {
    status.players.importing = true
    // Get all players from PG
    let client = new Client({
      host: process.env.PG_HOST,
      user: process.env.PG_USER,
      database: process.env.PG_DB,
      password: process.env.PG_PW,
    })
    await client.connect()
    let query = {
      text: 'SELECT "nickName", uid, created_at FROM players',
      rawMode: 'array',
    }
    const res = await client.query(query)
    client.end()
    status.players.total = res.rows.length
    res.rows = res.rows.map(row => {
      row.createdAt = (new Date(row.created_at)).getTime()
      row = Player.clean(row)
      return row
    })
    // Save all players to RehtinkDB
    await r.table(Player.table, { readMode: 'outdated' })
      .insert(res.rows, { conflict: 'replace', durability: 'soft' })
      .run()
    status.players.imported = res.rows.length
    status.players.importing = false
  }

  /**
   * Start importing players in background
   */
  public startPlayersImport () {
    setImmediate(this.importPlayers)
    return 'Started importing players'
  }

  /**
   * Start importing history in background
   */
  public startHistoryImport () {
    setImmediate(this.importHistory)
    return 'Started importing history'
  }

  public status () {
    return status
  }
}
