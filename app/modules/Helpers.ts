/**
 * Attaches EPOCH timestamp "createdAt" to object or objects in array
 *
 * @param data Data where to add current EPOCH timestamp
 *
 * @returns Data with createdAt attached
 */
export const addCreatedAt = (data: object | object[]): any | any[] => {
  if (!Array.isArray(data)) {
    data = [data]
  }
  const now = Date.now()
  // @ts-ignore // objet.map is not a function, but if above forces to array
  return data.map(el => {
    if (!el.createdAt) {
      el.createdAt = now
    }
    return el
  })
}

/**
 * Chunks array into array of arrays
 *
 * @param array Array to chunk into pieces
 * @param size Single chunk size
 *
 * @returns Chunked array
 */
export const chunk = (array: Array<any>, size: number): Array<any> => {
  const chunkedArr: Array<any[]> = []
  let index = 0
  while (index < array.length) {
    let chunk = array.slice(index, size + index) as Array<any>
    chunkedArr.push(chunk)
    index += size
  }
  return chunkedArr
}
