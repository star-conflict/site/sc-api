import Axios from 'axios'
import { Exception } from '@poppinss/utils'
import { RawToNormal } from './DataMapper'
import Player from 'App/models/Player'
import PlayerHistory from 'App/models/PlayerHistory'

/**
 * Raw player data from SC API
 */
export interface RawPlayer {
  /** In game name */
  nickName: string,
  /** Unique ID */
  uid: number
  /** Pilot rating */
  effRating?: number,
  /** Fleet power */
  prestigeBonus?: number,
  /** Pilot level */
  accountRank?: number,
  /** PvE stats */
  pve?: {
    /** Nr of games played */
    gamePlayed: number,
    /** Attack / power level */
    unlimPve_playerAttackLevel?: number,
    /** Defence level */
    unlimPve_playerDefenceLevel?: number,
    /** "Temple of Last hope" level */
    wavePve_maxWave?: number,
    /** Mission max levels */
    unlimPve_missionLevels?: {
      /** "Defence Contract" level */
      nalnifan_lumen_waves_T1?: number,
      /** "The price of trust" level */
      pve_empfrontier_waves_T1?: number,
      /** "Ariadne's Thread" level */
      bigship_building_2_easy?: number,
      /** "'Blackwood' Shipyard" level */
      bigship_building_easy?: number,
      /** "Operation 'Ice Belt'" level */
      asteroid_building_t1?: number,
      /** "Operation 'Monolith'" level */
      pve_desttown_waves_easy?: number,
      /** "Captured Dreadnought" level */
      capture_repairbase_t1?: number,
      /** "Fire support" level */
      planet_war_waves_T1?: number,
      /** "Processing rig" level */
      loot_geostation_normal?: number,
      /** "'Ellydium' plant raid" level */
      pve_frozen_station_t2?: number,
      /** "Operation «Crimson Haze»" level */
      pve_jericho_base_t2?: number,
      /** Pirate Fort Raid level */
      piratebay_hard?: number,
    },
  },
  /** PvP stats */
  pvp?: {
    /** Nr of games played */
    gamePlayed: number,
    /** Games won */
    gameWin?: number,
    /** Total assists */
    totalAssists: number,
    /** Time in battle in MS */
    totalBattleTime: number,
    /** Nr of deaths */
    totalDeath: number,
    /** Total damage done */
    totalDmgDone: number,
    /** Total healing done */
    totalHealingDone: number,
    /** Nr of kills */
    totalKill: number,
    /** Capture points captured */
    totalVpDmgDone: number,
  },
  /** Coop / vs AI stats */
  coop?: {
    /** Games won */
    gameWin?: number,
    /** Nr of games played */
    gamePlayed: number,
    /** Time in battle in MS */
    totalBattleTime: number,
  },
  /** Open space stats */
  openWorld?: {
    /** Karma number */
    karma?: number,
  },
  /** Clan stats */
  clan?: {
    /** Clan name */
    name: string,
    /** Clan tag */
    tag?: string,
    /** Clan unique ID */
    cid: number,
    /** Clan total PvP rating */
    pvpRating?: number,
    /** Clan total PvE rating */
    pveRating?: number,
  },
}

/**
 * SC API response
 */
export interface Response {
  /** Result status as string, "ok" or "error" */
  result: string
  /** Result status code as "0" - OK or "1" - error */
  code: number
  /** Response data */
  data?: RawPlayer
}

/**
 * Module for communicating with SC API
 */
export default class ScApi {
  /**
   * Gets player data directly from SC API.
   *
   * It's a lot faster than proxy, but might get IP banned for mass usage
   *
   * @param name Player ingame name
   */
  public static async fetchDirect (name: string): Promise<Player> {
    const response = await Axios.get(`${process.env.SC_API_URL}?nickname=${name}`)
      .then(({ data }) => data as Response)

    if (response.data) {
      let normalData = RawToNormal(response.data)
      await Player.createIfNotExists(normalData)
      await PlayerHistory.createIfNotExists(normalData)
      return normalData
    }
    throw new Exception(`No player found with name: ${name}`, 404, 'ERR_NO_PLAYER')
  }

  /**
   * Gets player data from SC API thro safe proxy
   *
   * Since proxy has up to 10 sec cold start time it might take longer to do request
   * But since proxy IP is constantly changing it's a lot safer to use for
   * mass requests
   *
   * @param name Player ingame name
   */
  public static async fetchProxy (name: string): Promise<Player> {
    const response = await Axios.get(`${process.env.SC_PROXY_URL}?name=${name}`)
      .then(({ data }) => data as Response)

    if (response.data) {
      let normalData = RawToNormal(response.data)
      await Player.createIfNotExists(normalData)
      await PlayerHistory.createIfNotExists(normalData)
      return normalData
    }

    throw new Exception(`No player found with name: ${name}`, 404, 'ERR_NO_PLAYER')
  }
}
