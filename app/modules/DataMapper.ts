import { RawPlayer } from './ScApi'
import Player from 'App/models/Player'

/**
 * Map raw player data to data with better naming convention
 *
 * @param raw Raw player data
 */
export const RawToNormal = (raw: RawPlayer): Player => {
  let mapped: any = raw
  if (raw.pve) {
    mapped.pve.attackLevel = raw.pve.unlimPve_playerAttackLevel
    mapped.pve.defenceLevel = raw.pve.unlimPve_playerDefenceLevel
    if (raw.pve.unlimPve_missionLevels) {
      mapped.pve.missionLevels = {
        defenceContract: raw.pve.unlimPve_missionLevels.nalnifan_lumen_waves_T1,
        thePriceOfTrust: raw.pve.unlimPve_missionLevels.pve_empfrontier_waves_T1,
        ariadnesThread: raw.pve.unlimPve_missionLevels.bigship_building_2_easy,
        blackwoodShipyard: raw.pve.unlimPve_missionLevels.bigship_building_easy,
        operationIceBelt: raw.pve.unlimPve_missionLevels.asteroid_building_t1,
        operationMonolith: raw.pve.unlimPve_missionLevels.pve_desttown_waves_easy,
        capturedDreadnought: raw.pve.unlimPve_missionLevels.capture_repairbase_t1,
        fireSupport: raw.pve.unlimPve_missionLevels.planet_war_waves_T1,
        processingRig: raw.pve.unlimPve_missionLevels.loot_geostation_normal,
        ellydiumPlantRaid: raw.pve.unlimPve_missionLevels.pve_frozen_station_t2,
        operationCrimsonHaze: raw.pve.unlimPve_missionLevels.pve_jericho_base_t2,
        pirateFortRaid: raw.pve.unlimPve_missionLevels.piratebay_hard,
        templeOfLastHope: raw.pve.wavePve_maxWave,
      }
      // Clean up remapped values
      delete mapped.pve.unlimPve_missionLevels
      delete mapped.pve.unlimPve_playerDefenceLevel
      delete mapped.pve.unlimPve_playerAttackLevel
      delete mapped.pve.wavePve_maxWave
    }
  }
  return mapped
}
