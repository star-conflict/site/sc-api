import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class BasicAuth {
  public async handle (ctx: HttpContextContract, next: () => Promise<void>) {
    const auth = ctx.request.header('authorization')
    if (!auth) {
      return ctx.response
        .status(401)
        .header('WWW-Authenticate', 'Basic realm="Secure Area"')
        .send('Login first')
    }

    const decoded = Buffer.from(auth.split(' ')[1], 'base64').toString()
    const username = decoded.split(':')[0]
    const password = decoded.split(':')[1]
    if (username === process.env.ADMIN_USER && password === process.env.ADMIN_PASSWORD) {
      return next()
    }

    return ctx.response
      .status(401)
      .header('WWW-Authenticate', 'Basic realm="Secure Area"')
      .send('Login first')
  }
}
