# Builder container
FROM node:alpine AS builder

# ENV NODE_ENV=production

WORKDIR /opt/builder

# Copy over package*.json files
COPY package*.json ./

RUN apk add --no-cache git

# Install packages
RUN npm i

# Copy over source code
COPY . .

ENV ENV_SILENT=true

RUN timeout 30 node ace build

RUN ls


# Make prod container
FROM node:alpine

# AdonisJS basics
ENV HOST=0.0.0.0
ENV PORT=80
ENV NODE_ENV=production
ENV ENV_SILENT=true
ENV APP_KEY=
# Proxy settings
ENV SC_PROXY_URL=
ENV SC_API_URL=
# Basic auth settings
ENV ADMIN_USER=
ENV ADMIN_PASSWORD=
# PG import settings
ENV PG_HOST=
ENV PG_USER=
ENV PG_DB=
ENV PG_PW=
# Main DB settings
ENV DB_NAME=
ENV DB_HOST=
ENV DB_PORT=

# Add working dir
WORKDIR /opt/sc-api

# Copy over package*.json files
COPY package*.json ./

COPY --from=builder /opt/builder/build .

RUN apk add --no-cache git

RUN npm ci --only=production

RUN ls

EXPOSE 80

CMD ["node", "server.js"]
