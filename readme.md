# Star-Conflict site API

<small>Non-official</small>

## Description

Star-Conflict API to recieve statistical information about players, corporations etc

## Contributing

- Clone repo: `git clone git@gitlab.com:star-conflict/site/sc-api.git`
- Install packages: `npm ci`
- Start with: `node ace serve --watch`
- Code
- Make PR
- ???
- Profit

TODO:

[ ] Add status checks http://adm.star-conflict.com/status
