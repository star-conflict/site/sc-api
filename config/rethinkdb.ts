const rethinkDb = {
  db: process.env.DB_NAME,
  host: process.env.DB_HOST,
  port: process.env.DB_PORT,
  // tls: true,
  // ca: caCert,
  // rejectUnauthorized: false,
}

export default rethinkDb
