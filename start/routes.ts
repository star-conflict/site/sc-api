/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes/index.ts` as follows
|
| import './cart'
| import './customer'
|
*/

import Route from '@ioc:Adonis/Core/Route'
import ScApi from 'App/modules/ScApi'

Route.get('/', async () => {
  return { hello: 'world' }
})

// Crawler routes
Route.get('/crawl/start', 'CrawlController.start')
  .middleware('BasicAuth')
Route.get('/crawl/status', 'CrawlController.status')
  .middleware('BasicAuth')

// Import routes
// Route.get('/import/players/start', 'ImportController.startPlayersImport')
// Route.get('/import/history/start', 'ImportController.startHistoryImport')
// Route.get('/import/status', 'ImportController.status')

// Proxy routes
Route.get('/proxy/secure/:id', ({ params }) => ScApi.fetchProxy(params.id))
Route.get('/proxy/plain/:id', ({ params }) => ScApi.fetchDirect(params.id))

// Player routes
Route.get('/players/history/:id', 'PlayersController.history')
Route.resource('/players/', 'PlayersController')
  .only(['index', 'show'])
